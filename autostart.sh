#!/bin/sh

xrdb -merge ${HOME}/.Xresources
xmodmap ${HOME}/.Xmodmap

[[ -z $(pgrep -f $(which xbindkeys)) ]] && $(which xbindkeys) &
[[ -z $(pgrep -f $(which dunst)) ]] && $(which dunst) &

$(which urxvt) -e screen -R -D -S local &
$(which urxvt) -name weechat -e weechat-curses &
$(which urxvt) -name taskwarrior -e taskwarrior-tui &
$(which urxvt) -name yt -e yt &
$(which urxvt) -name castero -e castero &
$(which urxvt) -name pyradio -e pyradio &
$(which urxvt) -name gtt -e gtt &
$(which firefox) &
$(which redshift-gtk) -l 41.9:13.3 -t 5700:3600 -g 0.8 -m randr -v &
$(which xss-lock) -- slock &
#$(which urxvt) -name toipe -e toipe &
#$(which urxvt) -name gtypist -e gtypist &
