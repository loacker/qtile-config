# Qtile config.py 
#
# Copyright (c) 2023 Matteo Piccinini
#
# SPDX-License-Identifier: MIT

from libqtile.config import Key, Screen, Group, Drag, Click, hook, Match
from libqtile.lazy import lazy
from libqtile import layout, bar, widget

import os
import subprocess
import re

from qtile_extras import widget as extras_widget
from qtile_extras.widget.decorations import RectDecoration


mod = "mod4"
alt = "mod1"
printkey = "Print"
shift = "shift"
control = "control"


# keys
keys = [
    # Switch between windows in current stack pane
    Key([mod], "k", lazy.layout.down()),
    Key([mod], "j", lazy.layout.up()),
    Key([mod], "h", lazy.layout.previous()),
    Key([mod], "l", lazy.layout.next()),

    # MonadTall and Stack layout
    Key([mod, shift], "k", lazy.layout.shuffle_up()),
    Key([mod, shift], "j", lazy.layout.shuffle_down()),
    Key([mod, shift], "h", lazy.layout.swap_left()),
    Key([mod, shift], "l", lazy.layout.swap_right()),
    Key([mod, shift], "space", lazy.layout.flip()),

    # Vertical and MonadTall layout
    Key([mod], "i", lazy.layout.grow()),
    Key([mod], "o", lazy.layout.shrink()),
    Key([mod, shift], 'm', lazy.layout.maximize()),
    Key([mod, shift], 'n', lazy.layout.normalize()),

    # Stack layout
    Key([mod, shift], "j", lazy.layout.client_to_next()),
    Key([mod, shift], "k", lazy.layout.client_to_previous()),
    Key([mod], "a", lazy.layout.add()),
    Key([mod], "d", lazy.layout.delete()),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod], "space", lazy.layout.toggle_split()),

    # Toggle floating
    Key([mod], "t", lazy.window.toggle_floating()),

    # Toggle full screen
    Key([mod], "f", lazy.window.toggle_fullscreen()),

    # Toggle minimize
    Key([mod], "m", lazy.window.toggle_minimize()),

    # This is usefull when floating windows get buried
    # Select floating window
    Key([alt], "Tab", lazy.group.next_window()),
    # Bring to front the buried window
    Key([alt], "grave", lazy.window.bring_to_front()),

    # Toggle between different layouts
    Key([mod], "Tab", lazy.next_layout()),
    Key([mod, shift], "Tab", lazy.prev_layout()),

    # Kill a window
    Key([mod], "w", lazy.window.kill()),

    # Restart or shutdown qtile
    Key([mod, control], "r", lazy.restart()),
    Key([mod, control], "q", lazy.shutdown()),

    # Run command
    Key([mod], "r", lazy.spawncmd()),

    # Dual monitor
    Key([mod, alt], "Tab", lazy.window.to_next_screen()),  # Doesn't work
    Key([mod, alt], "1", lazy.to_screen(0), lazy.group.toscreen(0)),
    Key([mod, alt], "2", lazy.to_screen(1), lazy.group.toscreen(1)),

    # Spin up applications
    Key([mod], "Return", lazy.spawn("urxvt")),
    Key([alt], "r", lazy.spawn("urxvt -name pyradio -e pyradio")),
    Key([alt], "c", lazy.spawn("urxvt -name weechat -e weechat-curses")),
    Key([alt], "y", lazy.spawn("urxvt -name yt -e yt")),
    Key([alt], "p", lazy.spawn("urxvt -name castero -e castero")),
    Key([alt], "t", lazy.spawn("urxvt -name gtt -e gtt")),
    Key([alt], "g", lazy.spawn("firefox")),
    Key([alt], "d", lazy.spawn("chromium")),
    Key([alt], "i", lazy.spawn("qutebrowser")),
    Key([alt], "w", lazy.spawn("urxvt -name taskwarrior -e taskwarrior-tui")),
    Key([alt], printkey, lazy.spawn("scrot -bs '%d-%m-%Y_%H-%M-%S_$wx$h_scrot_selection.png' -e 'mv $f ~/pictures/screenshots'")),
    Key([mod], printkey, lazy.spawn("scrot -ub '%d-%m-%Y_%H-%M-%S_$wx$h_scrot_window.png' -e 'mv $f ~/pictures/screenshots'")),
    Key([control], printkey, lazy.spawn("scrot '%d-%m-%Y_%H-%M-%S_$wx$h_scrot_fullscreen.png' -e 'mv $f ~/pictures/screenshots'")),
]


# groups
# "xprop WM_CLASS" command to retrieve the wm_class attribute of a window
groups = [
    Group("1", matches=[
        Match(wm_instance_class=re.compile(r"^(URxvt)$"))]),
    Group("2", matches=[
        Match(wm_class=re.compile(r"^(firefox)$")),
        Match(wm_class=re.compile(r"^(qutebrowser)$")),
        Match(title=re.compile(r"^(Chromium)$"))]),
    Group("3", matches=[
        Match(wm_class=re.compile(r"^(gtt)$")),
        Match(wm_instance_class=re.compile(r"^(weechat)$"))]),
    Group("4", matches=[
        Match(wm_class=re.compile(r"^(yt)$")),
        Match(wm_class=re.compile(r"^(castero)$")),
        Match(wm_class=re.compile(r"^(spotube)$")),
        Match(wm_class=re.compile(r"^(pyradio)$"))]),
    Group("5", matches=[
        Match(wm_class=re.compile(r"^(mpv)$")),
        Match(wm_class=re.compile(r"^(MPlayer)$")),
        Match(wm_class=re.compile(r"^(zoom)$")),
        Match(wm_class=re.compile(r"^(netflix)$"))]),
    Group("0", matches=[
        Match(wm_class=re.compile(r"^(toipe)$")),
        Match(wm_class=re.compile(r"^(gtypist)$")),
        Match(wm_class=re.compile(r"^(mattermost)$")),
        Match(wm_class=re.compile(r"^(ferdium)$")),
        Match(wm_class=re.compile(r"^(taskwarrior)$"))]),
]

for i in groups:
    # mod1 + number of group = switch to group
    keys.append(Key([mod], i.name, lazy.group[i.name].toscreen()))
    # mod1 + shift + number of group = switch to & move focused window to group
    keys.append(Key([mod, shift], i.name, lazy.window.togroup(i.name)))


# layouts
layouts = [
    layout.Max(),
    layout.MonadThreeCol(
        align=0,
        border_focus='FF0000',
        border_normal='000000',
        border_width=2,
        change_ratio=0.05,
        change_size=20,
        main_centered=True,
        margin=0,
        max_ratio=0.75,
        min_ratio=0.25,
        min_secondary_size=85,
        new_client_position='top',
        ratio=0.5,
        single_border_width=None,
        single_margin=None    
    ),
    layout.MonadTall(
        border_focus='FF0000',
        border_width=2
    ),
    layout.MonadWide(
        align=0,
        border_focus='FF0000',
        border_normal='000000',
        border_width=2,
        change_ratio=0.05,
        change_size=20,
        margin=0,
        max_ratio=0.75,
        min_ratio=0.25,
        min_secondary_size=85,
        new_client_position='after_current',
        ratio=0.5,
        single_border_width=None,
        single_margin=None    
    ),
    layout.VerticalTile(
        border_focus='FF0000',
        border_normal='000000',
        border_width=2
    ),
    layout.Stack(
        border_focus='FF0000',
        num_stacks=2,
        fair=True,
        border_width=2
    ),
    layout.Columns(
        border_focus='FF0000',
        border_normal='000000',
        border_on_single=False,
        border_width=2,
        fair=False,
        grow_amount=10,
        insert_position=0,
        margin_on_single=None,
        num_columns=2,
        split=True,
        wrap_focus_columns=True,
        wrap_focus_rows=True,
        wrap_focus_stacks=True
    ),
    layout.Matrix(
        border_focus='FF0000',
        border_normal='000000',
        border_width=2,    
        columns=2,
    ),
]


# qtile extras specific
decoration = {
    "decorations": [
	RectDecoration(
	    colour='495057',
	    radius=6,
	    filled=True,
	    padding_y=4,
	    group=True
	),
	RectDecoration(
	    colour='215578',
	    radius=6,
	    filled=False,
	    padding_y=4,
	    group=True
	)
    ],
    "padding": 6,
}


# widgets
widget_defaults = dict(
    font='Droid Sans Mono',
    fontsize=12,
    padding=3,
)

separator = widget.Sep(
    padding=10
)

groupbox = widget.GroupBox(
    use_mouse_wheel=False,
    borderwidth=2,
    this_current_screen_border='7B7FCC',
    this_screen_border='7B7FCC'
)

tasklist = widget.TaskList(
    title_width_method='uniform',
    txt_floating='[f] ',
    txt_maximized='[M] ',
    txt_minimized='[m] ',
    border='7B7FCC',
    unfocused_border='495057'
)

spacer_length5 = widget.Spacer(
    length=5
)

systray = widget.Systray()

volume = extras_widget.Volume(
    **decoration
)

clock = extras_widget.Clock(
    format='%a %b %d %H:%M:%S %Z %Y (%W)',
    **decoration
)

wifi = extras_widget.WiFiIcon(
    expanded_timeout=1
)

current_screen = widget.CurrentScreen()

current_layout = extras_widget.CurrentLayout(
    **decoration
)

backlight = extras_widget.Backlight(
    backlight_name='intel_backlight',
    **decoration
)

check_updates = extras_widget.CheckUpdates(
    distro='Arch_checkupdates',
    colour_have_updates='ffffff',
    no_update_string='',
    **decoration
)

prompt = widget.Prompt(
    padding=10,
    background='7b7fcc'
)

spacer = widget.Spacer()

net = extras_widget.Net(
    format=" {down:6.2f}{down_suffix:<2}\u2193 {up:6.2f}{up_suffix:<2}\u2191", 
    interface=None,
    use_bits=True,
    **decoration
)

df = extras_widget.DF(
    format='{uf}{m}|{r:.0f}%',
    partition='/',
    visible_on_warn=False,
    measure='G',
    warn_space=5,
    **decoration
)

hdd = widget.HDDBusyGraph(
    device='nvme0n1',
    border_color='7B7FCC',
    graph_color="00ff00"
)

swap = widget.SwapGraph(
    graph_color='f8ff95',
    border_color='7B7FCC'
)

mem = widget.MemoryGraph(
    graph_color='f0134d',
    border_color='7B7FCC'
)

cpu = widget.CPUGraph(
    graph_color='8bf5fa',
    border_color='7B7FCC'
)

temp = extras_widget.ThermalSensor(
    threshold=103,
    **decoration
)

battery = extras_widget.UPowerWidget(
    text_displaytime=1
)


# bars
top_bar = [
    groupbox,
    separator,
    tasklist,
    spacer_length5,
    systray,
    separator,
    volume,
    separator,
    clock,
    separator,
    wifi
]

bottom_bar = [
    current_screen,
    separator,
    current_layout,
    separator,
    backlight,
    separator,
    check_updates,
    prompt,
    spacer,
    separator,
    net,
    separator,
    df,
    separator,
    hdd,
    separator,
    swap,
    mem,
    separator,
    cpu,
    temp,
    separator,
    battery
]


# screens
screens = [
    Screen(
        top=bar.Bar(
            [
                groupbox,
                separator,
                tasklist,
                spacer_length5,
                systray,
                separator,
                volume,
                separator,
                clock,
                separator,
                wifi,
            ], 25,
        ),
        bottom=bar.Bar(
            [
                current_screen,
                separator,
                current_layout,
                separator,
                backlight,
                separator,
                check_updates,
                prompt,
                spacer,
                separator,
                net,
                separator,
                df,
                separator,
                hdd,
                separator,
                swap,
                mem,
                separator,
                cpu,
                temp,
                separator,
                battery,
            ], 25,
        ),
    ),
    Screen(
        top=bar.Bar(
            [
                groupbox,
                separator,
                tasklist,
                spacer_length5,
                separator,
                volume,
                separator,
                clock,
                separator,
                wifi,
            ], 25,
        ),
        bottom=bar.Bar(
            [
                current_screen,
                separator,
                current_layout,
                separator,
                backlight,
                separator,
                check_updates,
                prompt,
                spacer,
                separator,
                net,
                separator,
                df,
                separator,
                hdd,
                separator,
                swap,
                mem,
                separator,
                cpu,
                temp,
                separator,
                battery,
            ], 25,
        ),
    ),
]


# floating windows
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

@hook.subscribe.client_new
def floating(window):
    floating_types = ['notification', 'toolbar', 'splash', 'dialog']
    transient = window.window.get_wm_transient_for()
    if window.window.get_wm_type() in floating_types or transient:
        window.floating = True

floating_layout = layout.Floating(
    border_focus='FF0000',
    border_normal='000000',
    border_width=2,
    float_rules=[
        Match(wm_class='Display'),
        Match(title=re.compile(r"^meet.google.com.*$")),
    ]
)


# startup script
@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])


# configuration variable
dgroups_key_binder = None
dgroups_app_rules = []
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
auto_fullscreen = True
wmname = "Qtile"
reconfigure_screens = True


# debug
#def main(qtile):
#    qtile.cmd_debug()


# vim: set ts=8 sw=4 sts=4 tw=79 ff=unix ft=python et ai :
