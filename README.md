Qtile configuration
===================

My personal qtile configuration on Archlinux

![](screenshot.png)


Clone the repository:
---------------------

```
git clone git@github.com:loacker/my_qtile_config.git ~/.config/qtile
```

Required packages for a basic archlinux installation from repository:
---------------------------------------------------------------------

```
pacman -Sy qtile python-mpd2 python-iwlib python-pyxdg upower python-psutil python-dbus-next ttf-droid moc dbus-python
```

Required packages from AUR:
---------------------------

```
git clone https://aur.archlinux.org/qtile-extras.git
cd qtile-extras
makepkg -sir --skippgpcheck
```

or using pikaur

```
pikaur -S qtile_extras
```

Start qtile
-----------

```
cat > ~/.xinitrc << __EOF__
#!/bin/sh

exec qtile start
__EOF__

startx
```
